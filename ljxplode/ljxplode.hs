#!/usr/bin/runhaskell
-- ljxplode -- explode a LiveJournal XML export into separate files
-- Copyright (C) 2007 Claude Heiland-Allen
-- 
-- 
-- This program is free software; you can redistribute it and/or
-- modify it under the terms of the GNU General Public License
-- as published by the Free Software Foundation; either version 2
-- of the License, or (at your option) any later version.
-- 
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with this program; if not, write to the Free Software
-- Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.


module Main where

import System (getArgs)
import System.IO
import Distribution.Compat.FilePath

import Text.XML.HaXml.Parse
import Text.XML.HaXml.Combinators
import Text.XML.HaXml.Types
import Text.XML.HaXml.Escape
import Text.XML.HaXml.Verbatim
import Text.XML.HaXml.Pretty (element)
import Text.PrettyPrint.HughesPJ (render)

main = do
  files <- getArgs
  (sequence . map processFile) files
  putStrLn "done!"

processFile :: FilePath -> IO ()
processFile filename = do
  putStrLn ("R : " ++ filename)
  xml <- readFile filename
  (sequence . map writeEntryFile) (entries xml)
  return ()
    where
      entries xml = (livejournalf . getContent . xmlParse filename) xml
      getContent (Document _ _ e _) = CElem e
      livejournalf = deep (tag "entry")

writeEntryFile :: Content -> IO ()
writeEntryFile entry = do
  putStrLn ("W : " ++ itemid)
  h <- openFile (folder `joinFileName` filename `joinFileExt` "xhtml") WriteMode
  hPutStrLn h prologue
  ( hPutStrLn h . render . ppContent . style) entry
  hClose h
    where
      ppContent [CElem e] = element e
      itemid      = get "itemid"
      eventtime   = get "eventtime"
      logtime     = get "logtime"
      subject     = get "subject"
      event       = get "event"
      security    = get "security"
      allowmask   = get "allowmask"
      music       = get "current_music"
      mood        = get "current_mood"
      get t       = verbatim ((map unescape . keep /> tag t /> txt) entry)
      folder      = case security of
        "public"  -> "public"
        _         -> "private"
      filename    = map (whitelist filechars) title
      whitelist goodchars c
        | c `elem` goodchars = c
        | otherwise          = '_'
      filechars   = "-" ++ ['0'..'9'] ++ ['a'..'z'] ++ ['A'..'Z']
      unescape c  = case c of
        (CElem e) -> CElem (xmlUnEscape stdXmlEscaper e)
        otherwise -> c
      prologue = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n\
                 \<?xml-stylesheet href=\"style.css\" type=\"text/css\"?>\n\
                 \<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.1//EN\"\
                 \ \"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd\">"
      title       = eventtime ++ " - " ++ subject
      style =
        mkElemAttr "html" [("xmlns",    literal "http://www.w3.org/1999/xhtml"),
                           ("xml:lang", literal "en")]
          [ mkElem "head"
            [ mkElem "title"
              [ literal title ]
            ]
          , mkElemAttr "body" [("class", literal "entry")]
            [ mkElemAttr "h1" [("class", literal "title")]
              [ mkElemAttr "span" [("class", literal "date")]
                [ literal eventtime ]
              , literal " "
              , mkElemAttr "span" [("class", literal "subject")]
                [ literal subject ]
              ]
            , mkElemAttr "div" [("class", literal "event")]
              [ literal event ]
            , mkElemAttr "div" [("class", literal "current")]
              [ mkElemAttr "div" [("class", literal "music")]
                [ literal music ]
              , mkElemAttr "div" [("class", literal "mood")]
                [ literal mood ]
              ]
            , mkElemAttr "div" [("class", literal "valid")] 
              [ mkElemAttr "a" [("href", literal "http://validator.w3.org/check?uri=referer")]
                [ mkElemAttr "img" [("src", literal "http://www.w3.org/Icons/valid-xhtml11")
                                   ,("alt", literal "Valid XHTML 1.1")
                                   ,("height", literal "31")
                                   ,("width", literal "88")
                                   ] []
                ]
              , literal " "
              , mkElemAttr "a" [("href", literal "http://jigsaw.w3.org/css-validator/check/referer")]
                [ mkElemAttr "img" [("src", literal "http://jigsaw.w3.org/css-validator/images/vcss")
                                   ,("alt", literal "Valid CSS")
                                   ,("height", literal "31")
                                   ,("width", literal "88")
                                   ] []
                ]
              ]
            ]
          ]
