<?xml version="1.0" encoding="UTF-8"?>
<!--

cmcms == CMCMS Content Management System
Copyright (C) 2008,2009,2010,2012,2013,2016,2020  Claude Heiland-Allen

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" indent="no" encoding="UTF-8" />
<xsl:template match="/">
<xsl:apply-templates select="entrymonth/day/entry" />
</xsl:template>
<xsl:template match="entry">
<page>
<date><xsl:value-of select="substring(time, 0, 11)" />T<xsl:value-of select="substring(time, 12, 19)"/>+0000</date>
<file><xsl:value-of select="substring(time, 0, 11)" />_FIXME_filename</file>
<title><xsl:value-of select="subject" /></title>
<summary><div xmlns="http://www.w3.org/1999/xhtml">
<p>FIXME summary</p>
</div></summary>
<content><div xmlns="http://www.w3.org/1999/xhtml">
<xsl:value-of select="event" disable-output-escaping="yes" />
<div class="current">
<p class="music"><xsl:value-of select="music" /></p>
<p class="mood"><xsl:value-of select="mood" /></p>
<p class="avatar"><xsl:value-of select="pickeyword" /></p>
</div>
</div></content>
<tag>FIXME</tag>
</page>
</xsl:template>
</xsl:stylesheet>
