#!/bin/bash
set -e
me="$(dirname "$(readlink -e "$0")")"
for input in "${1:-.}"/????/??.xml
do
  output="${2:-.}/${input#${1:-.}/}"
  echo "R: ${input}"
  echo "W: ${output}"
  mkdir -p "$(dirname "${output}")"
  xsltproc "${me}/logjam2pages.xsl" - < "${input}" > "${output}"
done
