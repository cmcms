<?xml version="1.0" encoding="UTF-8"?>
<!--

cmcms == CMCMS Content Management System
Copyright (C) 2008,2009,2010,2012,2013,2022  Claude Heiland-Allen

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

-->
<xsl:stylesheet version="1.0"
 xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
>
<xsl:output method="html" encoding="UTF-8" />
<xsl:param name="URL" />
<xsl:param name="SITENAME" />
<xsl:param name="HEAD" />
<xsl:param name="HEADER1" />
<xsl:param name="HEADER2" />
<xsl:param name="FOOTER" />
<xsl:param name="tag" />
<xsl:template match="/">
<xsl:text disable-output-escaping='yes'>&lt;!DOCTYPE html&gt;
</xsl:text>
<html lang="en">
<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <title><xsl:value-of select="$tag" /> :: <xsl:value-of select="$SITENAME" /></title>
  <meta property="og:title" content="{$tag} :: {$SITENAME}" />
  <meta name="keywords" content="{$tag}" />
  <link rel="canonical" href="{$URL}{$tag}.html" />
  <meta property="og:url" content="{$URL}{$tag}.html" />
  <link rel="alternate" type="application/rss+xml" href="{$tag}.rss" title="{$SITENAME} {$tag} feed" />
  <meta name="generator" content="cmcms &lt;https://code.mathr.co.uk/cmcms&gt;" />
  <xsl:copy-of select="document($HEAD)/*/node()" />
</head>
<body>
  <header>
    <xsl:copy-of select="document($HEADER)/*/node()" />
  </header>
  <main>
    <h2><a class="feed-icon" href="{$tag}.rss" title="{$SITENAME} {$tag} feed"><img src="feed-icon.svg" alt="RSS" /></a><xsl:value-of select="$tag" /></h2>
    <xsl:apply-templates select="pages/page[tag=$tag]" />
    <div class="separator" />
  </main>
  <footer id="navigation">
    <xsl:copy-of select="document($RELATED)/*/node()" />
    <xsl:copy-of select="document($FOOTER)/*/node()" />
  </footer>
</body>
</html>
</xsl:template>
<xsl:template match="page">
<xsl:variable name="me" select="file" />
<article id="p_{file}">
<h3><a href="{file}.html"><xsl:value-of select="title" /></a></h3>
<ul class="tags">
<li><a class="date" href="#p_{$me}"><xsl:value-of select="date" /></a></li>
<xsl:for-each select="tag">
<li><a href="{.}.html#p_{$me}"><xsl:value-of select="." /></a></li>
</xsl:for-each>
</ul>
<xsl:copy-of select="summary/node()" />
</article>
</xsl:template>
</xsl:stylesheet>
