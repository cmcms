<?xml version="1.0" encoding="UTF-8"?>
<!--

cmcms == CMCMS Content Management System
Copyright (C) 2008,2009,2010,2012,2013,2016,2022  Claude Heiland-Allen

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

-->
<xsl:stylesheet version="1.0"
 xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
>
<xsl:output method="html" encoding="UTF-8" />
<xsl:param name="URL" />
<xsl:param name="SITENAME" />
<xsl:param name="HEAD" />
<xsl:param name="HEADER" />
<xsl:param name="FOOTER" />
<xsl:param name="NAVIGATION" />
<xsl:param name="RELATED" />
<xsl:param name="PAGE" />
<xsl:template match="/">
<xsl:text disable-output-escaping='yes'>&lt;!DOCTYPE html&gt;
</xsl:text>
<xsl:apply-templates select="pages/page[file=$PAGE]" />
</xsl:template>
<xsl:template match="page">
<xsl:variable name="top" select="/" />
<html lang="en">
<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <title><xsl:value-of select="title" /> :: <xsl:value-of select="$SITENAME" /></title>
  <meta property="og:title" content="{title} :: {$SITENAME}" />
  <meta name="description" content="{summary}" />
  <meta property="og:description" content="{summary}" />
  <meta name="keywords" content="{tag[1]} {tag[2]} {tag[3]} {tag[4]} {tag[5]}" />
  <link rel="canonical" href="{$URL}{$PAGE}.html" />
  <meta property="og:url" content="{$URL}{$PAGE}.html" />
  <meta name="generator" content="cmcms &lt;https://code.mathr.co.uk/cmcms&gt;" />
  <xsl:copy-of select="document($HEAD)/*/node()" />
</head>
<body>
  <header>
    <xsl:copy-of select="document($HEADER)/*/node()" />
  </header>
  <main>
    <article>
      <h2><xsl:value-of select="title" /></h2>
      <xsl:apply-templates select="content/node()" />
      <div class="separator" />
    </article>
  </main>
  <footer id="navigation">
    <xsl:apply-templates select="document($NAVIGATION)/navigation" />
    <xsl:copy-of select="document($RELATED)/*/node()" />
    <xsl:copy-of select="document($FOOTER)/*/node()" />
  </footer>
</body>
</html>
</xsl:template>
<xsl:template match="@*|node()">
<xsl:choose>
<xsl:when test="@class='gallery'">
<xsl:copy-of select="document(concat('gallery_', node(), '.xml'))" />
</xsl:when><xsl:otherwise>
<xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy>
</xsl:otherwise>
</xsl:choose>
</xsl:template>
<xsl:template match="navigation">
<nav class="related" aria-label="Related"><h3>related</h3>
  <ol role="list">
    <li><ol class="by-date">
      <li class="later"><xsl:choose>
<xsl:when test="by-date/later=''">&#9675;</xsl:when>
<xsl:when test="by-date/later!=''"><a href="{by-date/later}.html" title="later (by date)">&#8593;</a></xsl:when>
</xsl:choose></li>
      <li class="date"><a href="{substring(by-date/date,1,4)}.html#p_{$PAGE}" title="now (by date)"><xsl:value-of select="substring(by-date/date, 0, 17)" /></a></li>
      <li class="earlier"><xsl:choose>
<xsl:when test="by-date/earlier!=''"><a href="{by-date/earlier}.html" title="earlier (by date)">&#8595;</a></xsl:when>
<xsl:when test="by-date/earlier=''">&#9675;</xsl:when>
</xsl:choose></li>
    </ol></li>
<xsl:for-each select="by-tag">
    <li><ol class="by-tag">
      <li class="later"><xsl:choose>
<xsl:when test="later=''">&#9675;</xsl:when>
<xsl:when test="later!=''"><a href="{later}.html" title="later ({tag})">&#8593;</a></xsl:when>
</xsl:choose></li>
      <li class="tag"><a href="{tag}.html#p_{$PAGE}" title="now ({tag})"><xsl:value-of select="tag" /></a></li>
      <li class="earlier"><xsl:choose>
<xsl:when test="earlier!=''"><a href="{earlier}.html" title="earlier ({tag})">&#8595;</a></xsl:when>
<xsl:when test="earlier=''">&#9675;</xsl:when>
</xsl:choose></li>
    </ol></li>
</xsl:for-each>
  </ol>
</nav>
</xsl:template>
</xsl:stylesheet>
