#!/bin/bash

# cmcms == CMCMS Content Management System
# Copyright (C) 2008,2009,2010,2012,2013,2016,2017,2018,2019,2020,2021,2022,2023  Claude Heiland-Allen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
CMCMS="$(dirname "$(readlink -f "${0}")")"
shopt -s nullglob
if [[ ! ( "x${1}" == "xversion" || "x${1}" == "xinit" || "x${1}" == "xadd" || ( "x${1}" == "xgen" && "x${2}" != "x" && -f "${2}" ) ) ]]
then
  cat <<-USAGE
Usage:
    $(basename "${0}") help         -- display this usage information
    $(basename "${0}") version      -- display version information
    $(basename "${0}") init         -- initialize a new site
    $(basename "${0}") add          -- add a new page
    $(basename "${0}") gen [config] -- generate output site from "config"
USAGE
  exit $( [[ "x${1}" == "xhelp" ]] )
fi

case "${1}" in
  "version")
    cat <<-VERSION
cmcms 0.5
Copyright (C) 2008,2009,2010,2012,2013,2016,2017,2018,2019,2020,2021,2022,2023  Claude Heiland-Allen <claude@mathr.co.uk>
This is free software; see the source for copying conditions.  There is NO
warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
VERSION
    exit 0
    ;;
  "init")
    echo "$(basename "${0}"): init: not yet implemented, sorry"
    exit 1
    ;;
  "add")
    if [[ ! -d "pages/" ]]
    then
      echo "$(basename "${0}"): add: directory not found: \`pages/'"
      exit 1
    fi
    DATEFULL="$(date --iso=s)"
    DATESHORT="${DATEFULL:0:10}"
    read -e -p "date       : " -i "${DATEFULL}" PAGEDATE
    read -e -p "title      : " PAGETITLE
    read -e -p "url        : " -i "${DATESHORT}_$(echo "${PAGETITLE}" | sed "s|[^-_a-zA-Z0-9]|_|g" | tr "[:upper:]" "[:lower:]" | tr -s "_")" PAGEURL
    read -e -p "summary    : " PAGESUMMARY
    read -e -p "tag(s)     : " PAGETAGS
    read -e -p "gallery(s) : " PAGEGALLERIES
    PAGEFILE="pages/${PAGEURL}.xml"
    if [[ -f "${PAGEFILE}" ]]
    then
      echo "WARNING : file already exists: \`${PAGEFILE}'"
      PAGEFILE="pages/cmcms.$$.xml"
      if [[ -f "${PAGEFILE}" ]]
      then
        echo "ERROR   : file already exists: \`${PAGEFILE}'"
        exit 1
      fi
      echo "WARNING : writing alternative: \`${PAGEFILE}'"
    fi
    (
      cat <<-TEMPLATE
<page>
<date>${PAGEDATE}</date>
<file>${PAGEURL}</file>
<title>${PAGETITLE}</title>
<summary><div xmlns="http://www.w3.org/1999/xhtml">
<p>${PAGESUMMARY}</p>
</div></summary>
<content><div xmlns="http://www.w3.org/1999/xhtml">
<!-- delete this line and replace with your content -->
TEMPLATE
      for gallery in ${PAGEGALLERIES}
      do
        echo "<div class=\"gallery\">${gallery}</div>"
      done
      echo "</div></content>"
      for gallery in ${PAGEGALLERIES}
      do
        echo "<gallery>${gallery}</gallery>"
      done
      for tag in ${PAGETAGS}
      do
        echo "<tag>${tag}</tag>"
      done
      echo "</page>"
    ) > "${PAGEFILE}"
    echo "output saved to \`${PAGEFILE}', now edit it to add content"
    exit 0
    ;;
esac

# -------------------
echo "Configuring..."
# -------------------

source "${2}"

# make paths absolute
IDIR="$(dirname "${2}")"
if [ "x/" = "x${OUT:0:1}" ]
then
  ODIR="${OUT}"
else
  ODIR="$(dirname "${2}")/${OUT}"
fi
if [ "x/" = "x${TMP:0:1}" ]
then
  TDIR="${TMP}"
else
  TDIR="$(dirname "${2}")/${TMP}"
fi

# ------------------
echo "Setting up..."
# ------------------

mkdir -p "${ODIR}" "${TDIR}"
PAGES="${TDIR}/pages.xml"
(
  echo "<pages>" &&
  cat $( ls -1 --quoting-style=escape "${IDIR}/pages/"*.xml | sort -r ) &&
  echo "</pages>"
) > "${PAGES}"
INDEX="${TDIR}/index.txt"
(
  cat "${PAGES}" |
  xsltproc "${CMCMS}/extract.xsl" - &&
  echo "0000-00-00 END  END "
) | sort -nr > "${INDEX}"
(
  echo "<pages>" &&
  cat "${INDEX}" |
  head -n -1 |
  while read date file tags
  do
    cat "${IDIR}/pages/${file}.xml"
  done &&
  echo "</pages>"
) > "${PAGES}"


# ------------------------------------
echo "Generating common navigation..."
# ------------------------------------

(
  echo "<footer>"
  TAGS="${TDIR}/tags.txt"
  cat "${INDEX}" |
  head -n -1 |
  cut -d\  -f 3- |
  tr " " "\n" |
  grep -v "^$" |
  sort |
  uniq -c |
  sort -n |
  cat -n > "${TAGS}"
  total="$(cat "${TAGS}" | wc -l)"
  echo "<nav class=\"tagcloud\" aria-label=\"Tag Cloud\"><h3>tags</h3><ol role=\"list\">" &&
  cat "${TAGS}" |
  while read order count tag
  do
    class="$(( 10 * (order - 1) / total ))"
    echo "${tag} ${class}"
  done |
  sort |
  while read tag class
  do
    link="<a href=\"${tag}.html\">${tag}</a>"
    echo "<li class=\"p${class}\">${link}</li>"
  done &&
  echo "</ol></nav>"
  echo "<nav class=\"archives\" aria-label=\"Archives\"><h3>archives</h3><ol role=\"list\">" &&
  for year in $(seq 2023 -1 2005)
  do
    echo "<li><a href=\"${year}.html\">${year}</a></li>"
  done &&
  echo "<li><a href=\"everything.html\">*</a></li>" &&
  echo "</ol></nav>" &&
  echo "<nav class=\"recent\" aria-label=\"Recent\"><h3>recent</h3><ol role=\"list\">" &&
  cat "${INDEX}" | head -n-1 | head -n "${PERPAGE}" |
  while read date file tags
  do
    cat "${IDIR}/pages/${file}.xml" |
    xsltproc "${CMCMS}/recent.xsl" -
  done &&
  echo "</ol></nav>"
  echo "</footer>"
) > "${TDIR}/related.xml"

# ------------------------------------
echo "Generating header and footer..."
# ------------------------------------

(
  cat "${IDIR}/theme/head.xml"
) > "${TDIR}/head.xml"
(
  cat "${IDIR}/theme/header.xml"
) > "${TDIR}/header.xml"
(
  cat "${IDIR}/theme/footer.xml"
) > "${TDIR}/footer.xml"

# ----------------------------
echo "Generating galleries..."
# ----------------------------

cat "${PAGES}" |
xsltproc "${CMCMS}/galleries.xsl" - |
sort |
uniq |
while read gallery
do
  (
    echo "<div xmlns=\"http://www.w3.org/1999/xhtml\" class=\"gallery\">"
    ls -1 "${IDIR}/gallery/${gallery}/"*{.jpg,.png} |
    while read i
    do
      if [[ ! ( "${i}" =~ "_thumb.jpg" ) ]]
      then
        image="$(basename "${i}")"
        thumb="${image/%.jpg/_thumb.jpg}"
        thumb="${thumb/%.png/_thumb.jpg}"
        alt="${thumb/%_thumb.jpg/}"
        img="<img src=\"g/${gallery}/${thumb}\" alt=\"${alt}\" />"
        link="<a href=\"g/${gallery}/${image}\" title=\"${alt}\">${img}</a>"
        echo "<div>${link}</div>"
      fi
    done
    echo "</div>"
  ) > "${TDIR}/gallery_${gallery}.xml"
done

# --------------------------------
echo "Generating content pages..."
# --------------------------------

DATENEXT=""
DATETHIS=""
DATEPREV=""
PAGENEXT=""
PAGETHIS=""
PAGEPREV=""
TAGSTHIS=""
TAGSPREV=""
NAVIGATION="${TDIR}/navigation.xml"
cat "${INDEX}" |
while read date file tags
do
  DATENEXT="${DATETHIS}"
  DATETHIS="${DATEPREV}"
  DATEPREV="${date}"
  PAGENEXT="${PAGETHIS}"
  PAGETHIS="${PAGEPREV}"
  PAGEPREV="${file}"
  TAGSTHIS="${TAGSPREV}"
  TAGSPREV="${tags}"
  if [[ "x${DATEPREV}" == "x0000-00-00" || "x${PAGEPREV}" == "xEND" ]]
  then
    DATEPREV=""
    PAGEPREV=""
    TAGSPREV=""
  fi
  if [[ "x${PAGETHIS}" != "x" ]]
  then
    (
      cat <<-NAVIGATION
<navigation>
<by-date>
<later>${PAGENEXT}</later>
<date>${DATETHIS}</date>
<earlier>${PAGEPREV}</earlier>
</by-date>
NAVIGATION
      for tag in ${TAGSTHIS}
      do
        cat "${INDEX}" |
        grep "^[^ ]* [^ ]* .* ${tag} .*$" |
        grep -A 1 -B 1 "${PAGETHIS}" |
        (
          read dummy1 TAGFILE dummy2
          if [[ "x${TAGFILE}" == "x${PAGETHIS}" ]]
          then
            TAGNEXT=""
            TAGTHIS="${TAGFILE}"
            read dummy1 TAGFILE dummy2
            TAGPREV="${TAGFILE}"
          else
            TAGNEXT="${TAGFILE}"
            read dummy1 TAGFILE dummy2
            TAGTHIS="${TAGFILE}"
            read dummy1 TAGFILE dummy2
            TAGPREV="${TAGFILE}"
          fi
          cat <<-NAVIGATION
<by-tag>
<later>${TAGNEXT}</later>
<tag>${tag}</tag>
<earlier>${TAGPREV}</earlier>
</by-tag>
NAVIGATION
        )
      done
      echo "</navigation>"
    ) > "${NAVIGATION}"
    (
      cat "${PAGES}" |
      xsltproc \
        --path "${TDIR}" \
        --stringparam URL "${URL}" \
        --stringparam SITENAME "${NAME}" \
        --stringparam HEAD "head.xml" \
        --stringparam HEADER "header.xml" \
        --stringparam FOOTER "footer.xml" \
        --stringparam NAVIGATION "navigation.xml" \
        --stringparam RELATED "related.xml" \
        --stringparam PAGE "${PAGETHIS}" \
        "${CMCMS}/page.xsl" -
    ) >"${ODIR}/${PAGETHIS}.html"
  fi
done

# ----------------------------
echo "Generating tag pages..."
# ----------------------------

cat "${PAGES}" |
xsltproc "${CMCMS}/tags.xsl" - |
sort |
uniq |
while read tag ;
do
  cat "${PAGES}" |
  xsltproc \
    --path "${TDIR}" \
    --stringparam URL "${URL}" \
    --stringparam SITENAME "${NAME}" \
    --stringparam HEAD "head.xml" \
    --stringparam HEADER "header.xml" \
    --stringparam FOOTER "footer.xml" \
    --stringparam RELATED "related.xml" \
    --stringparam INFO "${IDIR}/tags/${tag}.xml" \
    --stringparam tag "${tag}" \
    "${CMCMS}/tag.xsl" - >"${ODIR}/${tag}.html"
done

# -----------------------------
echo "Generating year pages..."
# -----------------------------

for year in $(seq 2005 2023)
do
  cat "${PAGES}" |
  xsltproc \
    --path "${TDIR}" \
    --stringparam URL "${URL}" \
    --stringparam SITENAME "${NAME}" \
    --stringparam HEAD "head.xml" \
    --stringparam HEADER "header.xml" \
    --stringparam RELATED "related.xml" \
    --stringparam FOOTER "footer.xml" \
    --stringparam year "${year}" \
    "${CMCMS}/year.xsl" - >"${ODIR}/${year}.html"
done
pushd "${ODIR}"
rm -f "index.html"
ln -s "2023.html" "index.html"
popd

# ----------------------------------
echo "Generating everything page..."
# ----------------------------------

cat "${PAGES}" |
xsltproc \
  --path "${TDIR}" \
  --stringparam URL "${URL}" \
  --stringparam SITENAME "${NAME}" \
  --stringparam HEAD "head.xml" \
  --stringparam HEADER "header.xml" \
  --stringparam RELATED "related.xml" \
  --stringparam FOOTER "footer.xml" \
  "${CMCMS}/index.xsl" - >"${ODIR}/everything.html"

# --------------------------------
echo "Generating tag RSS feeds..."
# --------------------------------

cat "${PAGES}" |
xsltproc "${CMCMS}/tags.xsl" - |
sort |
uniq |
while read tag ;
do
  cat "${PAGES}" |
  xsltproc \
    --path "${TDIR}" \
    --stringparam TITLE "${tag} :: ${NAME}" \
    --stringparam URL "${URL}" \
    --stringparam DESCRIPTION "${NAME} ${tag} feed" \
    --stringparam TAG "${tag}" \
    "${CMCMS}/rss20tag.xsl" - >"${ODIR}/${tag}.rss"
done

# --------------------------------
echo "Generating main RSS feed..."
# --------------------------------

cat "${PAGES}" |
xsltproc \
  --path "${TDIR}" \
  --stringparam TITLE "${NAME}" \
  --stringparam URL "${URL}" \
  --stringparam DESCRIPTION "${NAME} news feed" \
  "${CMCMS}/rss20.xsl" - >"${ODIR}/index.rss"

# ------------------------------
echo "Copying static content..."
# ------------------------------

cp -au "${IDIR}/static/"* "${ODIR}/"

# ------------------------------
echo "Copying gallery images..."
# ------------------------------

mkdir -p "${ODIR}/g"
if ! [ "${ODIR}/g/" -ef "${IDIR}/gallery/" ]
then
  for g in "${IDIR}/gallery/"*
  do
    cp -au "${g}/" "${ODIR}/g/"
  done
fi

# -----------------------------------
echo "Updating gallery thumbnails..."
# -----------------------------------

if [[ "x${THUMBNAILER}" != "x" ]]
then
  for i in "${ODIR}/g/"*"/"*
  do
    if [[ ! ( "${i}" =~ "_thumb.jpg" ) ]]
    then
      if   [[ "${i}" =~ ".jpg" ]]
      then
        if [[ "${i/%.jpg/_thumb.jpg}" -ot "${i}" ]]
        then
          ${THUMBNAILER} "${i}" "${i/%.jpg/_thumb.jpg}"
        fi
      elif [[ "${i}" =~ ".png" ]]
      then
        if [[ "${i/%.png/_thumb.jpg}" -ot "${i}" ]]
        then
          ${THUMBNAILER} "${i}" "${i/%.png/_thumb.jpg}"
        fi
      fi
    fi
  done
fi

# ----------
echo "Done!"
# ----------
