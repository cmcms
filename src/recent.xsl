<?xml version="1.0" encoding="UTF-8"?>
<!--

cmcms == CMCMS Content Management System
Copyright (C) 2008,2010  Claude Heiland-Allen

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

-->
<xsl:stylesheet version="1.0"
 xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="text" encoding="UTF-8" />
<xsl:template match="/">
<xsl:text>&lt;li&gt;&lt;a href="</xsl:text>
<xsl:value-of select="page/file" />
<xsl:text>.html" title="</xsl:text>
<xsl:value-of select="page/title" />
<xsl:text>"&gt;</xsl:text>
<xsl:value-of select="page/title" />
<xsl:text>&lt;/a&gt;&lt;/li&gt;
</xsl:text>
</xsl:template>
</xsl:stylesheet>
