<?xml version="1.0" encoding="UTF-8"?>
<!--

cmcms == CMCMS Content Management System
Copyright (C) 2008,2009,2010  Claude Heiland-Allen

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" indent="no" encoding="UTF-8" />
<xsl:param name="TITLE"/>
<xsl:param name="DESCRIPTION"/>
<xsl:param name="URL"/>
<xsl:template match="/">
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
<channel>
<atom:link href="{$URL}index.rss" rel="self" type="application/rss+xml" />
<title><xsl:value-of select="$TITLE" /></title>
<link><xsl:value-of select="$URL" /></link>
<description><xsl:value-of select="$DESCRIPTION" /></description>
<xsl:for-each select="pages/page[position() &lt;= 12]">
<item>
<title><xsl:value-of select="title" /></title>
<description><xsl:text disable-output-escaping="yes">&lt;![CDATA[</xsl:text><xsl:apply-templates select="content/node()" /><xsl:text disable-output-escaping="yes">]]&gt;</xsl:text></description>
<link><xsl:value-of select="$URL" /><xsl:value-of select="file" />.html</link>
<guid><xsl:value-of select="$URL" /><xsl:value-of select="file" />.html</guid>
<pubDate><xsl:call-template name="date822"><xsl:with-param name="iso" select="date" /></xsl:call-template></pubDate>
<xsl:for-each select="tag">
<category><xsl:value-of select="." /></category>
</xsl:for-each>
</item>
</xsl:for-each>
</channel>
</rss>
</xsl:template>
<xsl:template name="date822"><xsl:param name="iso"/>
<xsl:variable name="year"  ><xsl:value-of select="substring($iso,1,4)"  /></xsl:variable>
<xsl:variable name="month" ><xsl:value-of select="substring($iso,6,2)"  /></xsl:variable>
<xsl:variable name="day"   ><xsl:value-of select="substring($iso,9,2)"  /></xsl:variable>
<xsl:variable name="hour"  ><xsl:value-of select="substring($iso,12,2)" /></xsl:variable>
<xsl:variable name="minute"><xsl:value-of select="substring($iso,15,2)" /></xsl:variable>
<xsl:variable name="second"><xsl:value-of select="substring($iso,18,2)" /></xsl:variable>
<xsl:variable name="zone"  ><xsl:value-of select="substring($iso,21,4)" /></xsl:variable>
<xsl:variable name="monstr"><xsl:value-of select="substring('JanFebMarAprMayJunJulAugSepOctNovDec',$month*3-2,3)" /></xsl:variable>
<xsl:variable name="zonstr">GMT</xsl:variable>
<xsl:value-of select="format-number($day,'00')"/><xsl:text> </xsl:text>
<xsl:value-of select="$monstr"/><xsl:text> </xsl:text>
<xsl:value-of select="$year"/><xsl:text> </xsl:text>
<xsl:value-of select="format-number($hour,'00')"/><xsl:text>:</xsl:text>
<xsl:value-of select="format-number($minute,'00')"/><xsl:text>:</xsl:text>
<xsl:value-of select="format-number($second,'00')"/><xsl:text> </xsl:text>
<xsl:value-of select="$zonstr" />
</xsl:template>
<xsl:template match="@*|node()">
<xsl:choose>
<xsl:when test="@class='gallery'">
<xsl:copy-of select="document(concat('gallery_', node(), '.xml'))" />
</xsl:when><xsl:otherwise>
<xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy>
</xsl:otherwise>
</xsl:choose>
</xsl:template>
</xsl:stylesheet>
